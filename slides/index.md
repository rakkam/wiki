# Slides

Allows to declare a list of slides that can be then displayed in a slideshow / carousel.

## Syntax

```
:: slides
- uri: image-1.jpg
  caption: "Lorem ipsum _dolor_ sit amor"
- uri: image-2.jpg
  caption "Lorem ipsum"
::
```