
# Button

A button is a clickable element with an icon and a label (both optional). It is associated with an intent, to be processed by the handlers which have been registered for it.


## Example (draft)

```
:: button
label: Share
icon: /icons/share.svg
intent: ["share", "my-resource"]
::
```


---

:: data
is-a: type
::