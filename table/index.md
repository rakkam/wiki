
# Table

A table consists of content arranged in columns and rows.


## Syntax example (draft)



```
:: table
spans: {mobile: [12], tablet: [3, 9]}
-
cell1
-
cell2
::
```
