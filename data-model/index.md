# Model

The `model` tag allows to define a data model.

## Syntax example

```
:: model
- type: Book
  property: title: String
  property: authors: Person[]
  property: publication-date?: Date
- type: Person
  property: firstName: String
  property: lastName: String
  property: birthDate?: Date
::
```

## Resources

* [Defining Schemas for Property Graphs by using the GraphQL Schema Definition Language](http://olafhartig.de/files/HartigHidders_GRADES2019_Preprint.pdf) - Olaf Hartig, Jan Hidders
