# Nuxt Stories

https://dev.to/richardeschloss/introduction-to-story-driven-development-with-nuxt-stories-1j33

>You want your ideas to come to life as you think of them. The instant you write down those ideas, you demand instant gratification from instant functionality. nuxt-stories is a module that you can use to rapidly write stories, game-plans, notes, documentation, whatever for your Nuxt web application. 

https://github.com/richardeschloss/nuxt-stories