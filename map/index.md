# Map

A map represents localized data on a zoomable background.

## Syntax example

```
:: map
center: [42, 44]
tiles: https://openstreetmap.org/tiles/
layers:
  - name: "Museums"
    query: "select title, lat, lng from museums"
::    
```
