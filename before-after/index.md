# Before after

This component allows typically to compare two images over time using a slider.

## Syntax example

```
:: before-after
- uri: image-1.jpg
- uri: image-2.jpg
::
```