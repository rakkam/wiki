# Tabs

## Syntax example

```
:: tabs
- label: Tab 1
  intent: ["open-page", "page-1"]
- label Tab 2
  intent: ["open-page", "page-2"]
```

