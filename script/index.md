
# Script

A script is a code block to be processed by a code interpreter (e.g. JavaScript runtime).

## Example


```
:: script
data = searchService.query("select title, caption, uri from photos")
::
```
