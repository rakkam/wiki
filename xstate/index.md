# XState

* https://github.com/davidkpiano/xstate
* http://slides.com/davidkhourshid/finite-state-machines
  * "User interfaces are graphs"
  * "State machines provide a common language for designers & developers."

## Syntax

```
id: 'light',
initial: 'green',
context: { redLights: 0 },
states: {
  green: {
    on: {
      TIMER: 'yellow'
    }
  },
  yellow: {
    on: {
      TIMER: {
        target: 'red',
        actions: () => console.log('Going to red!')
      }
    }
  },
  red: {
    entry: assign({ redLights: (ctx) => ctx.redLights + 1 }),
    on: {
      TIMER: 'green'
    }
  }
}
```

Source: https://xstate.js.org/docs/packages/xstate-fsm/#example

:: data
is-a: software
is-linked-to: finite-state-machine
::