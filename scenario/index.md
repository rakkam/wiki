# Scenario

Allows to describe a function of a system. Can be used to generate automated tests, documentation, ...

```
:: scenario
protocol: http
version: 1
method: get
request: 
response:
::
```

---

:: data
is-a: type
::