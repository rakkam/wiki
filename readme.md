# Rakkam – Rakkam's A Knife Knew Application Markup

The wiki syntax was introduced in [1995](https://en.wikipedia.org/wiki/History_of_wikis) by Ward Cunningham to ease the composition of hypertext. Nowadays, the wiki syntax is used not only to compose static web pages but also dynamic ones comprising interactive components. However, to the best of our knowledge, no common markup syntax exists for describing entire applications ranging from data model, UI, to states. Rakkam is pursuing this goal, together with advancing th state of the art in mixing data and code for creating interactive notebooks.

## Goals

* A set of compatible domain specific languages for:
  * Data models: structure, constraints, etc.
  * User interfaces: components, layout, intents, ...
  * Application states and transitions
* DSL runtimes in multiple contexts: web apps, native apps, console apps, voice apps, ...
* Test generators for multiple frameworks
* Documentation generators including with visuals

## Why

* Mixing text and code is key for modern documents such as scientific thesis, journal articles involving statistics, contracts, etc.
* Using a markup rather than "code" will make applications more dynamic: there should be no compilation step required.
* Writing applications using a declarative and simple domain specific language (DSL) rather than more complex Turing-complete languages such as JavaScript or Python is likely to make the developed applications easier to maintain, and easier to deploy in variable contexts. For example, a "vocal" runtime could be provided in order to generate an interface that targets specifically blind people, while another runtime would generate a visual application.

## Components

* Syntax for declaring and configuring components within a Markdown text
* Syntax for interlinking documents: import / include / dependencies
* Vocabularies for visual components, events, states
* Parsers
* Runtimes: React, Vue, terminal, ...
* Repository of dynamic documents

## Scope

The following aspects should be covered by the language:
* Component mapping: ability to associate a tag to a component
* Context: ability for components to have access to the execution context in which they are getting executed (e.g. the current URL, their position in the document, etc.)
* Intents: ability to define actions in an abstract manner, whose effective processing is left to the available components handling the target intents.
* Framework agonsticism: ability to execute the language with various component frameworks such as Angular, React, Vue and others, and various layout frameworks such as Bootstrap, Bulma, Tailwind and others.
* Templating: ability to use several template engines such as Moustache, Handlebar, doT etc.

## Links

* https://en.wikipedia.org/wiki/List_of_document_markup_languages
* [Defining Schemas for Property Graphs by using the GraphQL Schema Definition Language](http://olafhartig.de/files/HartigHidders_GRADES2019_Preprint.pdf) - Olaf Hartig, Jan Hidders
* https://en.wikipedia.org/wiki/Curl_(programming_language)
* https://en.wikipedia.org/wiki/Comparison_of_document-markup_languages

## Alternative names

* Amarkar
* Markee
* Samarkand


