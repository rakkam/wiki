# Swagger

"Swagger is in essence an Interface Description Language for describing RESTful APIs expressed using JSON. Swagger is used together with a set of open-source software tools to design, build, document, and use RESTful web services. Swagger includes automated documentation, code generation (into many programming languages), and test-case generation." https://en.wikipedia.org/wiki/Swagger_(software)

## Syntax



---

:: data
is-a: DSL
see-also: open-api
::
