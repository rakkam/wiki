# Cat

The `cat` tag allows to include a page into another (transclusion).

## Syntax

```
:: cat
uri: my-page
context: self
::
```

